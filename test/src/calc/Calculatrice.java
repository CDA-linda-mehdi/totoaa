package calc;

public class Calculatrice {
	
	
	
	public static int addition(int a, int b) {
		return a + b;
	}
	
	
	public static int soustraction(int a, int b) {
		return a - b;
	}
	
	
	public static int multiplication(int a, int b) {
		return a * b;
	}
	
	
	public static int max(int a, int b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}
	
	
	public static char signe(int a) {
		if (a >= 0) {
			return '+';
		} else {
			return '-';
		}
	}
}