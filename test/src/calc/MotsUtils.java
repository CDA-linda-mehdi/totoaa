package calc;

public class MotsUtils {

	public static String inverser(String str) {
		String a = "";

		for (int i = str.length() -1; i>= 0; i--) {
			a = a + str.charAt(i);

		}
		return a;

	}

	public static String caracteresCommuns(String str, String str2) {
		String res = "";

		for (int i = 0; i < str.length(); i++) {
			
			char leCaractereEnCours = str.charAt(i);
			
			for(int j = 0; j<str2.length(); j++) {

				if (leCaractereEnCours == str2.charAt(j)) {
					
					int indexCaractereEnCourDansRes = res.indexOf(leCaractereEnCours);
					
					if(indexCaractereEnCourDansRes == -1) {
						res = res + leCaractereEnCours;
					}
					
				}
				
			}

		}
		return res;
	}
	
	// autre m�thode :
	/*
	public static String caracteresCommuns(String str, String str2) {

        String strf ="";
        boolean bool = true;

        for(int i =0; i<str.length(); i++) {
            bool = true;
            for(int j =0; bool && j<strf.length(); j++) {
                if(str.charAt(i) != strf.charAt(j)) {
                    bool = true;
                }
                else {
                    bool = false;
                }
            }
            if(bool){
                for(int k =0; bool && k<str2.length(); k++) {
                    if(str.charAt(i)==str2.charAt(k)) {
                        strf = strf + str.charAt(i);
                        bool = false;
                    }
                }
            }
        }


        return strf;
    }
	*/
	
	/*
	public static Boolean estUnPalindrome(String str) {
		String str = "";
		for (int i = 0; i < str.length(); i++) {
			char leCaractereEnCoursDebut = str.charAt(i);
			for (int j = str.length() -1; j < str.length(); j--) {
				char leCaractereEnCoursFin = str.charAt(j);
				if (leCaractereEnCoursDebut == leCaractereEnCoursFin) {
					return true;
				} else {
					return false;
				}
			}
		}
	}
	*/
	
	// autres m�thodes :
	
	public static boolean estUnPalindrome(String a) {
		return a.equals(MotsUtils.inverser(a));
	}
	
	
	/*
	   public static boolean estUnPalindrome(String str) {

        String strInverse ="";
        boolean bool = true;
        
        for(int j=0; bool && j<(str.length()/2);j++) {
            if(str.charAt(j) != str.charAt(str.length() - j -1)) {
                bool = false;
            }
        }
        
        return bool;
    }
	 */
	
	
	/*
	   public static boolean estUnPalindrome (String str) {


        int z=str.length()-1;
        boolean res=true;

        for (int a=0;a<z&&res;a++) {
            if (str.charAt(a) != str.charAt(z)) {
                res = false;
            }
            z--;
        }
        return res;
    }
}
	 */
	
	public static long sommeChiffresDansMot(String str) {
		return 0;
		
	}


}
